#pragma once

#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

array<cv::Point, 2> getLinesEnd(int i, int j, int w, float tang)
{
	array<cv::Point, 2> result;

	if (tang >= -1 && tang <= 1)
	{
		result[0] = cv::Point(i, (-w / 2) * tang + j + w / 2);
		result[1] = cv::Point(i + w, (w / 2) * tang + j + w / 2);
	}
	else
	{
		result[0] = cv::Point(i + w / 2 + w / (2 * tang), j + w / 2);
		result[1] = cv::Point(i + w / 2 - w / (2 * tang), j - w / 2);
	}

	return result;
}
Mat visualizeAngles(Mat img, Mat mask, float** angles, int w)
{
	Mat result = Mat::zeros(img.rows, img.cols, CV_8UC3);
	float mask_threshold = pow(w - 1, 2);
	for (int j = 1; j < img.rows; j += w)
	{
		for (int i = 1; i < img.cols; i += w)
		{
			float radian = 10000;
			if (radian > mask_threshold)
			{
				float tang = tan(angles[((i - 1) / w)][((j - 1) / w)]);
				array<cv::Point, 2> points = getLinesEnd(i, j, w, tang);
				line(result, points[0], points[1], 150);
			}
		}
	}
	resize(result, result, Size(img.cols * 3, img.rows * 3));
	return result;
}

pair < Mat, vector<pair<float, float>>> calculatesAngles(Mat img, Mat mask, int w)
{
	pair < Mat, vector<pair<float, float>>> result;
	vector<pair<float, float>> vec;
	Mat Ix, Iy;

	float** angles = new float* [img.cols / w];
	for (int i = 0; i < img.cols / w; ++i)
		angles[i] = new float[img.rows / w];


	Sobel(img, Ix, img.depth(), 1, 0);
	Sobel(img, Iy, img.depth(), 0, 1);

	//imshow("Gradient X", Ix);
	//imshow("Gradient Y", Iy);

	for (int j = 1; j < img.rows; j += w)
	{
		for (int i = 1; i < img.cols; i += w)
		{
			float nominator = 0.0f;
			float dominator = 0.0f;

			for (int l = j; l < min(j + w, img.rows - 1); ++l)
			{
				for (int k = i; k < min(i + w, img.cols - 1); ++k)
				{
					float Gx = round(Ix.data[l * img.cols + k]);
					float Gy = round(Iy.data[l * img.cols + k]);

					nominator += 2 * Gx * Gy;
					dominator += pow(Gx, 2) - pow(Gy, 2);
				}
			}

			if (nominator != 0 && dominator != 0)
			{
				float angle = (CV_PI + atan2(nominator, dominator)) / 2;

				int ln = sqrt(2 * pow(w, 2)) / 2;

				float dx = cos(angle - CV_PI / 2.0f);
				float dy = sin(angle - CV_PI / 2.0f);

				vec.push_back({ dx, dy });

				angles[(i - 1) / w][(j - 1) / w] = angle;
			}
			else
				angles[(i - 1) / w][(j - 1) / w] = 0;

		}
	}

	Mat dst = visualizeAngles(img, mask, angles, w);

	result.first = dst;
	result.second = vec;

	return result;
}
