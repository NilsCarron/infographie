#pragma once

#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

enum MINUTAE_TYPE
{
	ENDING,
	BIFURCATION,
	NONE
};

struct Minutae_point
{
	Minutae_point()
		: x(0), y(0), type(MINUTAE_TYPE::NONE)
	{}
	Minutae_point(int x, int y, MINUTAE_TYPE type)
		: x(x), y(y), type(type)
	{}

	int x;
	int y;
	MINUTAE_TYPE type;
};

MINUTAE_TYPE minutae_at(Mat pixels, int i, int j, int kernel_size)
{
	vector<pair<int, int>> cells;
	vector<int> values;

	if (pixels.at<uchar>(i, j) == 0.0f)
	{
		if (kernel_size == 3) {
			cells = { {-1, -1}, {-1, 0}, {-1,  1},
						{0, 1}, {1, 1}, {1, 0},
						{1, -1}, {0, -1}, {-1, -1} };
		}
		else
		{
			cells = { {-2, 2}, {-2, -1}, {-2, 0}, {-2, 1}, {-2, 2},
						{-1, 2}, {0, 2}, {1, 2}, {2, 2}, {2, 1}, {2, 0},
						{2, -1}, {2, -2}, {1, -2}, {0, -2}, {-1, -2}, {-2, -2} };
		}

		for (int k = 0; k < cells.size(); ++k)
		{
			int x = cells[k].first;
			int y = cells[k].second;

			values.push_back(pixels.at<uchar>(i + x, j + y));
		}

		int crossings = 0;
		for (int k = 0; k < values.size() - 1; ++k)
			crossings += abs(values[k] - values[k + 1]);
		crossings /= 2;

		if (crossings == 1)
			return MINUTAE_TYPE::ENDING;
		if (crossings == 3)
			return MINUTAE_TYPE::BIFURCATION;

	}

	return MINUTAE_TYPE::NONE;
}
pair<Mat, vector<Minutae_point>> calculateMinutae(Mat src, int kernel_size = 3)
{
	pair<Mat, vector<Minutae_point>> result;
	vector<Minutae_point> minutae_points;

	//Binarisation
	Mat binaryImg;
	threshold(src, binaryImg, 10, 1, THRESH_BINARY);

	Mat minutar_mat;
	cvtColor(src, minutar_mat, COLOR_GRAY2RGB);

	for (int j = kernel_size; j < src.rows - kernel_size / 2; ++j)
	{
		for (int i = kernel_size; i < src.cols - kernel_size / 2; ++i)
		{
			MINUTAE_TYPE minutae_type = minutae_at(binaryImg, j, i, kernel_size);
			if (minutae_type == MINUTAE_TYPE::NONE) continue;

			if (minutae_type == MINUTAE_TYPE::ENDING)
				circle(minutar_mat, { i, j }, 1, { 150, 0, 0 }, 0.5f);
			else if (minutae_type == MINUTAE_TYPE::BIFURCATION)
				circle(minutar_mat, { i, j }, 1, { 0, 150, 0 }, 0.5f);

			Minutae_point mp = Minutae_point(i, j, minutae_type);
			minutae_points.push_back(mp);
		}
	}

	result.first = minutar_mat;
	result.second = minutae_points;

	return result;
}

//TODO:
void minutae_selection(); // NIST Barycenter method