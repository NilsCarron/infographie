#pragma once

#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

float calculateSD(Mat img)
{
	float sum = 0.0, mean, standardDeviation = 0.0;
	int data_size = img.rows * img.cols;
	int i;

	for (i = 0; i < data_size; ++i)
		sum += img.data[i];

	mean = sum / data_size;

	for (i = 0; i < data_size; ++i)
		standardDeviation += pow(img.data[i] - mean, 2);

	return sqrt(standardDeviation / data_size);
}
Mat segmentation(Mat img, int w, float t)
{

	float threshold = calculateSD(img) * t;

	cout << "THRESHOLD: " << threshold << endl;

	Mat segmented_image = img.clone();

	Mat image_variance = Mat::zeros(img.rows, img.cols, CV_8UC1);
	Mat mask = Mat::ones(img.rows, img.cols, CV_8UC1);


	for (int j = 0; j < img.rows; j += w)
	{
		for (int i = 0; i < img.cols; i += w)
		{
			int box[4] = { i, j, min(w, img.cols - i), min(w, img.rows - j) };
			Rect roi(box[0], box[1], box[2], box[3]);
			float block_stddev = calculateSD(img(roi));
			image_variance(roi) = Scalar(block_stddev);

		}
	}

	for (int i = 0; i < img.rows * img.cols; i++)
	{
		if (image_variance.data[i] < threshold)
			mask.data[i] = 0;
		if (mask.data[i] == 1)
			mask.data[i] = 255;
	}

	Mat kernel = getStructuringElement(MORPH_ELLIPSE, cv::Point(w * 2, w * 2));
	cv::morphologyEx(mask, mask, MORPH_OPEN, kernel);
	cv::morphologyEx(mask, mask, MORPH_CLOSE, kernel);

	for (int i = 0; i < img.rows * img.cols; i++)
	{
		segmented_image.data[i] *= mask.data[i];
	}

	//imshow("mask", mask);

	return mask;
}