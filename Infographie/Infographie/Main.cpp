#include <iostream>
#include <opencv2/opencv.hpp>

#include "Thin.h"

#include "fuzzyvault/fuzzy.h"

#include "Segmentation.h"
#include "Orientation.h"
#include "Gabor.h"
#include "Minutae.h"
#include "FuzzyVault.h"


using namespace std;
using namespace cv;

const int BLOCK_IMG = 8;

//ENCODING & DECODING
string encoding(Mat src);
bool decoding(Mat src);

const string ORIGINAL_KEY;

int main(int argc, char* argv[])
{
	Mat img = imread("img/test.BMP", IMREAD_GRAYSCALE);
	resize(img, img, { img.cols * 2, img.rows * 2 });

	string key = encoding(img);

	cout << key << endl;

	return 0;
}

string encoding(Mat src)
{
	
	std::string result;
	
	Mat norm_img;
	normalize(src, norm_img, 255, 190, NORM_MINMAX, -1, noArray());

	Mat mask = segmentation(norm_img, BLOCK_IMG/3, 0.20f);
	pair<Mat, vector<pair<float, float>>> cangles = calculatesAngles(norm_img, mask, BLOCK_IMG);

	Mat gaborMat = gabor(src, cangles.second, BLOCK_IMG);
	auto cminutae = calculateMinutae(gaborMat);
	vector<Minutae_point> minutae_points = cminutae.second;
	

	/*DEBUG
	*/
	imshow("orientation", cangles.first);
	imshow("src", src);
	imshow("Thin", gaborMat);
	imshow("Minutae", cminutae.first);

	waitKey();
	

	/*
	VAULT orignal_vault = lock("GARFIELD", minutae_points);

	vector<float> coeffs = unlock(minutae_points, orignal_vault);

	result = decode(coeffs);
	
	cout << "FUZZY VAULT DECODING: " << result << endl;

	*/

	return result;
} 

bool decoding(Mat src)
{
	return true;
}

/*
// Encoding

string input_string =
	"{\n"
	"  \"setSize\": 9,\n"
	"  \"corpusSize\": 7776,\n"
	"  \"correctThreshold\": 6\n"
	"}";

string original_word = "[ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]";

string params = fuzzy_vault::gen_params(input_string);
string secret = fuzzy_vault::gen_secret(params, original_word);

std::cout << secret << endl;

string original_keys = fuzzy_vault::gen_keys(secret, original_word, 1);

//Decoding

string test_word = "[ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]";
string recovery_keys = fuzzy_vault::gen_keys(secret, test_word, 1);
if (recovery_keys == original_keys)
	std::cout << "Original Keys" << std::endl;

//waitKey();
*/