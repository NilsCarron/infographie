#pragma once

#include "Minutae.h"
#include <vector>
#include <string>
#include <random>

#include "PolyFit.h"

using namespace std;

const int DEGREE = 8;
const int T = 10;
const int R = 40;

typedef vector<pair<float, float>> VAULT;

vector<float> get_coefficients(string word)
{
	vector<float> coeffs;
	int n = 1;

	vector<string> substrings;
	for (int i = 0; i < word.size(); ++i)
	{
		substrings.push_back(word.substr(i, n));
	}

	float num;
	for (auto it = substrings.begin(); it != substrings.end(); ++it)
	{
		num = 0;
		for (int i = 0; i < (*it).size(); ++i) {
			num += (int)(*it)[i] * pow(100.0f, i);
		}
		coeffs.push_back(pow(num, 0.333f));
	}

	return coeffs;
}

float p_x(float x, vector<float> coeffs)
{
	float y = 0;
	int degree = coeffs.size() - 1;

	for (auto &coeff : coeffs)
	{
		y += pow(x, degree) * coeff;
		degree -= 1;
	}

	return y;
}

float max_template(vector<Minutae_point> points)
{
	float max = points[0].x > points[0].y ? points[0].x : points[0].y;
	for (int i = 1; i < points.size(); ++i)
	{
		if (points[i].x > max)
			max = points[i].x;
		if (points[i].y > max)
			max = points[i].y;
	}
	return max;
}
float max_vault(VAULT vault)
{
	float max = vault[0].second;
	for (int i = 1; i < vault.size(); ++i)
	{
		if (vault[i].second > max)
			max = vault[i].second;
	}
	return max;
}

VAULT lock(string word_to_encode, vector<Minutae_point> minutae_points)
{
	VAULT vault;
	vector<float> coeffs = get_coefficients(word_to_encode);

	for (auto& coeff : coeffs)
	{
		cout << "[ " << coeff << " ]" << endl;
	}

	cout << "Calculate Vault" << endl;
	for (auto& mp : minutae_points)
	{
		vault.push_back({mp.x, p_x(mp.x, coeffs)});
		//vault.push_back({ mp.y, p_x(mp.y, coeffs) });
	}

	/* NOISE CHAFF POINT
	cout << "Calculate Chaff Point" << std::endl;
	float max_x = max_template(minutae_points);
	float max_y = max_vault(vault);

	auto rng = default_random_engine{};
	for(int i = T; i < R; ++i)
	{
		float x_i = uniform_real<float>(0, max_x * 1.1)(rng);
		float y_i = uniform_real<float>(0, max_y * 1.1)(rng);
		vault.push_back({ x_i, y_i });
	}


	shuffle(vault.begin(), vault.end(), rng);
	*/
	return vault;
}

float approx_equal(float x, float y, float epsilon)
{
	return abs(x - y) < epsilon;
}

pair<float, float> project(float x, VAULT vault)
{
	for (auto& v : vault)
	{
		if (approx_equal(x, v.first, 0.001))
			return { x, v.second };
	}

	return { -1, -1 };
}

std::vector<float> unlock(vector<Minutae_point> minutae_points, VAULT vault)
{
	std::vector<float> x;
	std::vector<float> y;

	for (auto &mp : minutae_points)
	{
		pair<float, float> result = project(mp.x, vault);
		if (result.first != -1 && result.second != -1)
		{
			x.push_back(result.first);
			y.push_back(result.second);
		}
		/*
		result = project(mp.y, vault);
		if (result.first != -1 && result.second != -1)
		{
			x.push_back(result.first);
			y.push_back(result.second);
		}
		*/
	}

	return polyfit_boost(x, y, DEGREE);
}

string decode(std::vector<float> coeffs)
{
	string result = "";

	for(auto &coeff : coeffs)
	{
		std::cout << "{ " << coeff << " }" << std::endl;
		int num = int(round(pow(coeff, 3)));
		if (num == 0) continue;
		while(num > 0)
		{
			result += tolower(char(num % 100));
			num /= 100;
		}
	}

	return result;
}