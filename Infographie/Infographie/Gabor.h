#pragma once

#include "opencv2/opencv.hpp"
#include "Thin.h"

using namespace std;
using namespace cv;

Mat gabor(Mat src, vector<pair<float, float>> vec, int w)
{
	Mat dst = Mat::zeros(src.rows, src.cols, CV_32F);

	int size = 1;
	double sigma = 5;
	double theta = 0;
	double lambd = 7;
	double gamma = 24;
	double psi = 0;
	
	int index = 0;
	for (int j = 1; j < src.rows; j += w)
	{
		for (int i = 1; i < src.cols; i += w)
		{
			float dx = vec[index].first;
			float dy = vec[index].second;

			theta = atan2(dy, dx) + CV_PI / 2;

			Mat tmp;
			Mat gabor = getGaborKernel( { size, size }, sigma, theta, lambd, gamma, psi);
			filter2D(src, tmp, CV_32F, gabor);

			int temp_size = w - 1;
			if (src.cols < i + temp_size)
				temp_size = (src.cols - 1) - i;
			if (src.rows < j + w - 1 && temp_size >(src.rows - 1) - j)
				temp_size = (src.rows - 1) - j;

			for (int l = 0; l < src.rows; l++) {
				for (int k = 0; k < src.cols; k++) {
					if (j <= l && l <= j + temp_size && i <= k && k <= i + temp_size)
						dst.at<float>(l, k) = tmp.at<float>(l, k);
				}
			}

		}

		index++;
	}

	dst.convertTo(dst, CV_8U);
	adaptiveThreshold(dst, dst, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, 5);

	//imshow("gabor", dst);

	//Thinning
	thin(dst, true, true, true);

	return dst;
}